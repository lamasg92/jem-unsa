<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"utf-8">
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="keywords" content="Jornadas de Enseñanza de la Matemática, Educación, Salta">
        <meta name="description" content="Pagina con información sobre las Jornadas de Enseñanza de la Matemática. Agosto 2017.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        
       <!-- <link rel="shortcut icon" href="./img/logoCris.ico"> -->

        <title>III Jornadas de Ense&ntilde;anza de la Matem&aacute;tica</title>
        <!-- CSS -->
        <link rel="stylesheet" href="./css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="./css/bootstrap-social.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
            
        <script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "5a95d64d-01e6-457d-a708-52d2053032eb", 
doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
        
    
    </head>
    
    

    <body>
    
    

    <!-- Barra principal de Navegación -->
        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="./index.html">Principal</a></li>
                    <li class="active"><a href="./Talleres2018.html">Talleres</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <!-- Fin de la Barra de Navegación-->
    <!-- Header -->
        <div class="jumbotron">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-8">            
                        <h1>III Jornadas de Enseñanza de la Matemática</h1>
                        <p>Departamento de Matemática - Facultad de Ciencias Exactas - UNSa.</p>
                        <p>Del 1 al 3 de agosto de 2018</p>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <img class="img-responsive" alt="logo curso" src="./img/logo2018.png" width="400">                    </div>
              </div>
            </div>
        </div>
        <!--/container -->
  <footer class="footer">
        <div class="container">
            
        </div>
        </br>
        <ul>
    <div class="list-inline">
                <li><span class="copyright">III Jornadas de Enseñanza de Matemática</span> - 2018</li>
                <li><a href="mailto:jornadasmatematicasalta@gmail.com ?subject=jem 2016" ><strong>Contactanos</strong></a><span class="dot divider"> &middot;</span></li>
    </div>
    </footer>           
    <!-- fin de footer -->
</body>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/jquery-2.1.1.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</html>