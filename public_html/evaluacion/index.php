<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"utf-8">
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="keywords" content="Jornadas de Enseñanza de la Matemática, Educación, Salta">
        <meta name="description" content="Pagina con información sobre las Jornadas de Enseñanza de la Matemática. Agosto 2017.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        
       <!-- <link rel="shortcut icon" href="./img/logoCris.ico"> -->

        <title>Admin JEM</title>
        <!-- CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="../css/bootstrap-social.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
            
        <script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "5a95d64d-01e6-457d-a708-52d2053032eb", 
doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>    
    </head>
    <body>
        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>
        <div class="jumbotron">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-8">            
                        <h2>III Jornadas de Enseñanza de la Matemática</h2>
                        <p>Departamento de Matemática - Facultad de Ciencias Exactas - UNSa.</p>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <img class="img-responsive" alt="logo curso" src="../img/logo2018.png" width="200"></div>
              </div>
            </div>
        </div>


<!-----------------------L...O...G...I...N------------------------------->
<div class="row">
	<div class="col-md-6 col-md-offset-3 ">
		<form action="ingresar.php" method="get">
			USUARIO: <input class="form-control" type="number" name="dni" id="dni">
			<br>
            CONTRASEÑA: <input class="form-control" type="password" name="pass" id="pass">
            <br>
			<input class="btn btn-primary" type="submit" name="Submit" value="Ingresar" >	
		</form>
	</div>
</div>

<!-----------------------L...O...G...I...N------------------------------->


 <footer class="footer">
        <div class="container">
            
        </div>
        </br>
        <ul>
    <div class="list-inline">
                <li><span class="copyright">III Jornadas de Enseñanza de Matemática</span> - 2018</li>
                <li><a href="mailto:jornadasmatematicasalta@gmail.com ?subject=jem 2016" ><strong>Contactanos</strong></a><span class="dot divider"> &middot;</span></li>
    </div>
    </footer>           
    <!-- fin de footer -->
</body>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./js/jquery-2.1.1.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</html>