<?php
require('../admin/Evaluacion.php');
$evaluaciones=new Evaluacion();
if (isset($_GET["user"]) and $_GET["user"]<>""){
$pass = $_GET["user"];
}
if (isset($_GET["id"]) and $_GET["id"]<>""){
$id = $_GET["id"];
}
$evaluacion=$evaluaciones->getEvaluacion($id);
$propuesta=$evaluaciones->getPropuestaEvaluacion($evaluacion['propuesta_id']);
$preguntas=$evaluaciones->getPreguntas();
include('main.php');
?>

<div class="col-md-8 col-md-offset-2">
<p>
<h3>Titulo:
<?php 
echo $propuesta['titulo']."</h3>"; ?>
</p>

<h3> Abstract:</h3>
<?php echo '<p align="justify">'.$propuesta['abstract'].'</p>'; ?>

<p>
<h3>Enlace:
<?php echo '<a target="_blank" href="../aceptados/'.$propuesta['documento'].'">Documento</a></h3>' ?>
</p>

<form action="updateEvaluacion.php" method="post">

	<div class="col-md-12">
<table class="display table table-hover" cellspacing="0" width="100%">
        <thead>
             <th>Pregunta</th>
             <th>si/no</th>
             <th>Respuesta</th>
        </thead>

        <tbody>
    <?php while ($fila=mysqli_fetch_array($preguntas)){
    	echo "<tr><td>";
      $respuesta=$evaluaciones->getRespuesta($fila['id'],$evaluacion['id']);
    	echo $fila['pregunta']."</td><td>";
  		echo '<select class="" name="sino[]" value="">';
      if ($respuesta['sino']=='si'){
            echo '<option value="si" selected>SI</option>
        		<option value="no" >NO</option>';
      }else{
            echo '<option value="si">SI</option>
            <option value="no" selected>NO</option>';
      }
	    echo '</select></td><th>';
  		echo '<input name="id_preg[]" type="hidden" value="'.$fila['id'].'">';
      echo '<input name="idpe[]" type="hidden" value="'.$respuesta['id'].'">';
  		echo '<input class="form-control col-md-8" name="respuesta[]" type="text" value="'.$respuesta['respuesta'].'" ></th></tr>';
    	}
    echo '<input name="pass" type="hidden" value="'.$pass.'">' ;
    ?>
    </tbody>
    </table>
    <label class="control-label">Esta propuesta es:</label>
 	<?php echo '<select class="" name="estado" value="">';
        if ($evaluacion['estado']=='aprobado') {
          echo '<option value="aprobado" selected>Aprobada</option>
                <option value="reprobado" >Reprobada</option>';
        } else {
          echo '<option value="aprobado" >Aprobada</option>
                <option value="reprobado" selected>Reprobada</option>';
        }
        
	
	     echo '</select>';
	echo '<input name="id_eva" type="hidden" value="'.$evaluacion['id'].'">';
	?>
<br>
<label class="control-label">Sugerencia:</label>
<?php
	 echo '<textarea class="form-control" rows="10" cols="60" name="sugerencia">'.$evaluacion['sugerencia'].'</textarea>';
?>

<br><br>
<button class="btn btn-primary" type="submit" name="Submit" onclick="return confirm('¿Seguro de su respuesta?')">Guardar cambios</button>
</div>
</form>
</div>