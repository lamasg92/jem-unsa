<?php
require('conexion.php');

Class Distribucion extends Conexion{
	public function Distribucion(){
		parent::__construct();

	define('consultaMaestra', ' FROM oferentes AS of 
					INNER JOIN oferentespropuestas AS ofpr ON ofpr.oferente_id=of.id
                    INNER JOIN propuestas AS pr ON ofpr.propuesta_id=pr.id 
				    INNER JOIN areaspropuestas AS arpr ON arpr.propuesta_id=pr.id 
				    INNER JOIN areas AS ar ON arpr.area_id=ar.id 
				    INNER JOIN areasjurados AS arju ON arju.area_id=ar.id 
				    INNER JOIN jurados AS ju ON arju.jurado_id=ju.id
				    WHERE of.dni!=ju.dni AND pr.estado LIKE "%aceptado%"  ');
	}

	public function getAsignados(){
		$consulta='SELECT pr.id as idp,pr.titulo
				    FROM propuestas AS pr 
				    WHERE pr.estado LIKE "%aceptado%" 
				    ORDER BY idp';
		$tabla=$this->conexion_db->query($consulta);
		return $tabla;
	}

	public function Aprobado($id){
		$consulta='SELECT count(e.estado) as cant FROM propuestasjurados AS pj 
					INNER JOIN evaluaciones AS e ON e.propuestajurado_id=pj.id 
					WHERE e.estado LIKE "%aprobado%" AND pj.propuesta_id='.$id;
		$query=$this->conexion_db->query($consulta);
		$resultado=$query->fetch_array();
		return $resultado['cant'];
	}

	public function Reprobado($id){
		$consulta='SELECT count(e.estado) as cant FROM propuestasjurados AS pj 
					INNER JOIN evaluaciones AS e ON e.propuestajurado_id=pj.id 
					WHERE e.estado LIKE "%reprobado%" AND pj.propuesta_id='.$id;
		$query=$this->conexion_db->query($consulta);
		$resultado=$query->fetch_array();
		return $resultado['cant'];
	}

	public function getEvaluacion($idp,$idj){
		$consulta='SELECT e.estado FROM propuestasjurados AS pj 
					INNER JOIN evaluaciones AS e ON e.propuestajurado_id=pj.id 
					WHERE pj.propuesta_id='.$idp.' and pj.jurado_id='.$idj;
		$query=$this->conexion_db->query($consulta);
		$resultado=$query->fetch_array();
		return $resultado['estado'];
	}

	public function getJuradosPropuesta($id){
		$consulta='SELECT ju.id as idj, ju.nombre,ju.email,prju.evaluar,ju.id,prju.id as idpj
					FROM propuestasjurados AS prju 
				    INNER JOIN jurados AS ju ON prju.jurado_id=ju.id
				    WHERE prju.propuesta_id='.$id.'
				    ORDER BY ju.id';
		$tabla=$this->conexion_db->query($consulta);
		return $tabla;
	}

	private function getTabla($id){//busca todos los posibles jurados para una propuesta
		$consulta='SELECT pr.id AS prid,ju.id AS juid '.consultaMaestra.'
				    and pr.id='.$id.' and ju.cant<3
				    GROUP BY pr.id,ju.id
				    ORDER BY RAND() LIMIT 1' ;
		$tabla=$this->conexion_db->query($consulta);
		return $tabla;
	}

	 public function cantAreas($id_pr,$id_ju)
	{
		$consulta='SELECT count(ar.id) as cant '.consultaMaestra.' AND pr.id='.$id_pr.' AND ju.id='.$id_ju;
		$query=$this->conexion_db->query($consulta);
		$areas=$query->fetch_array();
		return $areas['cant'];
	}

	private function getOptimoArea($id,$area){//trae propuestas de una determinada area
		$consulta='SELECT pr.id AS prid,ju.id AS juid '.consultaMaestra.'
				    and pr.id='.$id.' and ar.id='.$area.' and ju.cant<3
				    GROUP BY pr.id,ju.id
				    ORDER BY RAND() LIMIT 1' ;
		$tabla=$this->conexion_db->query($consulta);
		return $tabla;
	}

	private function getJuradoOptimo($id){//busca un jurado otimo para la propuesta
		$consulta='SELECT pr.id AS prid,ju.id AS juid,count(ar.id) as cant '.consultaMaestra.'
				    and pr.id='.$id.' and ju.cant<3
                    GROUP BY pr.id,ju.id
                    ORDER BY cant DESC LIMIT 1 ';
		$tabla=$this->conexion_db->query($consulta);
		return $tabla;
	}

	private function cantidadJurados($id){//cantidad de jurados que aceptaron o estan en espera
		$consulta='SELECT count(id) as cant FROM propuestasjurados WHERE propuesta_id='.$id.' AND (evaluar LIKE "%aceptar%" OR evaluar is NULL)';
		$query=$this->conexion_db->query($consulta);
		$jurados=$query->fetch_array();
		return $jurados['cant'];
	}

	private function verifica($id_pr,$id_ju){//verifica la exitencia o no de la relacion de jurado y propuesta
		$consulta='SELECT * FROM propuestasjurados WHERE propuesta_id='.$id_pr.' AND jurado_id='.$id_ju.' AND (evaluar LIKE "%aceptar%" OR evaluar is NULL)';
		$query=$this->conexion_db->query($consulta);
		$jurados=$query->fetch_array();
		if (count($jurados)>0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private function designa($jurados){//inserta los jurados en la base de datos
		while ($jurado=mysqli_fetch_array($jurados) ) { 
			if ($this->verifica($jurado['prid'],$jurado['juid'])==FALSE AND $this->cantidadJurados($jurado['prid'])<3) {
				$consultaAux='INSERT INTO propuestasjurados(id, jurado_id, propuesta_id) 
				VALUES (NULL,'.$jurado['juid'].','.$jurado['prid'].')';		
				$query=$this->conexion_db->query($consultaAux);
				$consultaAux2='UPDATE jurados SET cant=cant+1 WHERE id='.$jurado['juid'];
				$query=$this->conexion_db->query($consultaAux2);
			}
		}
	}

	public function designacionOptima(){//designa solo un jurado a cada propuesta el mas optimo(mas areas en comun)
		$query=$this->conexion_db->query('SELECT count(id) as cant FROM propuestas');
		$propuestas=$query->fetch_array();
		$cant=$propuestas['cant'];

		while ($cant>0) {
			$jurados=$this->getJuradoOptimo($cant);
			$this->designa($jurados);
			$cant=$cant-1;
			
		}
	}

	public function designarPorArea(){//primero asigna jurados de areas raras
		$areas=$this->conexion_db->query('SELECT * FROM areas ORDER BY rareza DESC');//ordena areas por rareza
		while ($area=mysqli_fetch_array($areas) ) { 
			$consulta='SELECT p.id FROM propuestas AS p 
						INNER JOIN areaspropuestas AS ap ON ap.propuesta_id=p.id 
						WHERE ap.area_id='.$area['id'];
			$propuestas=$this->conexion_db->query($consulta);//busca las propuestas de esas areas
			while ($propuesta=mysqli_fetch_array($propuestas) ) { 
				$jurados=$this->getOptimoArea($propuesta['id'],$area['id']);//busca los jurados de esas areas y propuestas
				$this->designa($jurados);
			}
		}
	}

	public function designacionAleatoria(){//designa un jurado aleatorio
		$query=$this->conexion_db->query('SELECT count(id) as cant FROM propuestas');
		$propuestas=$query->fetch_array();
		$cant=$propuestas['cant'];

		while ($cant>0) {
			$jurados=$this->getTabla($cant);
			$this->designa($jurados);
			$cant=$cant-1;
			}
	}

	public function limpiar(){//pone en cero los contadores de jurados
	$query=$this->conexion_db->query('SELECT count(id) as cant FROM jurados');
		$propuestas=$query->fetch_array();
		$cant=$propuestas['cant'];
		while ($cant>0) {
				$consultaAux='UPDATE jurados SET cant=0 WHERE id='.$cant;
				$query=$this->conexion_db->query($consultaAux);
				$cant=$cant-1;
			}
		}

}

?>
