<?php
require('conexion.php');

Class Area extends Conexion{
	public function Area(){
		parent::__construct();
	}

	public function getArea(){
		$areas=$this->conexion_db->query('SELECT * FROM areas');
		return $areas;
	}

	public function cantArea(){
		$query=$this->conexion_db->query('SELECT * FROM areas');
		$cant=0;
		while ($fila=mysqli_fetch_array($query) ) {
			$cant=$cant+1;
		}
		return $cant;
	}

	public function rareza(){//calcula que tan raraes una determinada area valor alto mas raro
		$consulta='SELECT a.id, COUNT(a.id) as cant FROM areas as a INNER JOIN areasjurados as aj on a.id=aj.area_id group by a.id ORDER BY a.id DESC';
		//$areas=$this->conexion_db->query($consulta);
		$cont=$this->cantArea();
		while ($cont>0) {
			$consultaAux='UPDATE areas SET rareza=1 WHERE id='.$cont;
			$query=$this->conexion_db->query($consultaAux);
			$cont=$cont-1;
		}
		$total=0;
			$areas=$this->conexion_db->query($consulta);
		   while ($area=mysqli_fetch_array($areas) ) {
				$total=$total+$area['cant'];
			}
			$areas=$this->conexion_db->query($consulta);
		   while ($area=mysqli_fetch_array($areas) ) {
				$aux=1-($area['cant']/$total);
				$consultaAux2='UPDATE areas SET rareza='.$aux.' WHERE id='.$area['id'];
				$query=$this->conexion_db->query($consultaAux2);
			}
	}
}

?>