<?php
require('conexion.php');

Class Propuesta extends Conexion{
	public function Propuesta(){
		parent::__construct();
	}

	public function getPropuesta(){
		$propuestas=$this->conexion_db->query('SELECT * FROM propuestas');
		return $propuestas;
	}

	public function getPropuestaId($id){
		$query=$this->conexion_db->query('SELECT * FROM propuestas WHERE id='.$id);
		$propuesta=$query->fetch_array();
		return $propuesta;
	}

	public function pass($pass){
		$query=$this->conexion_db->query('SELECT * FROM jurados WHERE pass='.$pass);
		$des=$query->fetch_array();
		$dni=($pass/(39-$des['id']))-20180801;
		return $dni;
	}

	public function updatePropuesta($consulta){
		$query=$this->conexion_db->query($consulta);
	}

	public function updateName(){
		$propuestas=$this->conexion_db->query('SELECT * FROM propuestas');
		   while ($propuesta=mysqli_fetch_array($propuestas) ) {
		   		$id=$propuesta['id'];
		   		if ($id<10){
					$consulta = 'UPDATE propuestas SET documento="A-0'.$id.'-JEM2018.pdf" WHERE id='.$id;
				}else{
					$consulta = 'UPDATE propuestas SET documento="A-'.$id.'-JEM2018.pdf" WHERE id='.$id;
				}
				$query=$this->conexion_db->query($consulta);
			}
	}

	public function updatePropuestaJurado($id,$evaluar){
		$consulta = 'UPDATE propuestasjurados SET evaluar="'.$evaluar.'" WHERE id='.$id;
		$query=$this->conexion_db->query($consulta);
		if($evaluar==aceptar){
			$query=$this->conexion_db->query('INSERT INTO evaluaciones (propuestajurado_id) VALUES ('.$id.')');
		}
	}
	
	public function getPropuestaJurado($dni,$id){
		$consulta= 'SELECT pj.evaluar,pj.id FROM jurados AS j inner join propuestasjurados  as pj on j.id=pj.jurado_id inner join propuestas as p on pj.propuesta_id=p.id WHERE j.dni='.$dni.' AND p.id='.$id ;
		$query=$this->conexion_db->query($consulta);
		$propuesta=$query->fetch_array();
		return $propuesta;
	}
}

?>