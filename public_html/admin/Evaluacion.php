<?php
require('conexion.php');

Class Evaluacion extends Conexion{
	public function Oferente(){
		parent::__construct();
	}

	public function getEvaluaciones(){
		$evaluaciones=$this->conexion_db->query('SELECT * FROM evaluaciones');
		return $evaluaciones;
	}

	public function getEvaluacion($id){
		$consulta='SELECT e.propuestajurado_id,e.sugerencia,e.estado,e.id,pj.propuesta_id,pj.jurado_id 
					FROM evaluaciones as e INNER JOIN propuestasjurados as pj ON pj.id=e.propuestajurado_id
					WHERE e.id='.$id;
		$query=$this->conexion_db->query($consulta);
		$evaluacion=$query->fetch_array();
		return $evaluacion;
	}

	public function getRespuesta($idp,$ide){
		$query=$this->conexion_db->query('SELECT * FROM evaluacionespreguntas WHERE evaluacion_id='.$ide.' AND pregunta_id='.$idp);
		$respuesta=$query->fetch_array();
		return $respuesta;
	}

	public function getPropuestaEvaluacion($idPropuesta){
		$query=$this->conexion_db->query('SELECT * FROM propuestas WHERE id='.$idPropuesta);
		$propuesta=$query->fetch_array();
		return $propuesta;
	}

	public function getPropuestaJurado($id){
		$consulta='SELECT e.propuestajurado_id,e.sugerencia,e.estado,e.id,pj.propuesta_id,pj.jurado_id 
					FROM evaluaciones as e INNER JOIN propuestasjurados as pj ON pj.id=e.propuestajurado_id
					WHERE pj.id='.$id;
		$query=$this->conexion_db->query($consulta);
		$evaluacion=$query->fetch_array();
		return $evaluacion;
	}

	public function getJuradoEvaluacion($idJurado){
		$query=$this->conexion_db->query('SELECT * FROM jurados WHERE id='.$idJurado);
		$jurado=$query->fetch_array();
		return $jurado;
	}

	public function getPreguntas(){
		$preguntas=$this->conexion_db->query('SELECT * FROM preguntas');
		return $preguntas;
	}

	public function updateEvaluacion($id,$sugerencia,$estado){
		$consulta = 'UPDATE evaluaciones SET sugerencia="'.$sugerencia.'",estado="'.$estado.'" WHERE id='.$id;
		$query=$this->conexion_db->query($consulta);
	}

	public function insertPreguntas($evaluacion_id,$pregunta_id,$respuesta,$sino){
		$consulta='INSERT INTO evaluacionespreguntas(id, respuesta, sino, evaluacion_id, pregunta_id) VALUES (NULL,"'.$respuesta.'","'.$sino.'",'.$evaluacion_id.','.$pregunta_id.')';
		$query=$this->conexion_db->query($consulta);
	}

	public function updatePreguntas($id,$evaluacion_id,$pregunta_id,$respuesta,$sino){
		$consulta='UPDATE evaluacionespreguntas SET respuesta="'.$respuesta.'", sino="'.$sino.'" WHERE id='.$id.' AND evaluacion_id='.$evaluacion_id.' AND pregunta_id='.$pregunta_id ;
		echo $consulta.'<br>';
		$query=$this->conexion_db->query($consulta);
	}
}

?>