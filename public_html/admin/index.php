<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"utf-8">
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="keywords" content="Jornadas de Enseñanza de la Matemática, Educación, Salta">
        <meta name="description" content="Pagina con información sobre las Jornadas de Enseñanza de la Matemática. Agosto 2017.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        
       <!-- <link rel="shortcut icon" href="./img/logoCris.ico"> -->

        <title>Admin JEM</title>
        <!-- CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="../css/bootstrap-social.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
            
        <script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "5a95d64d-01e6-457d-a708-52d2053032eb", 
doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
        
    
    </head>
    
    

    <body>
    
    

    <!-- Barra principal de Navegación -->
        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="./index.php">Principal</a></li>
                    <li class="active"><a href="./jurados/index.php">Jurados</a></li>
                    <li class="active"><a href="./oferentes/index.php">Oferentes</a></li>
                    <li class="active"><a href="./areas/index.php">Areas</a></li>
                    <li class="active"><a href="./propuestas/index.php">Propuestas</a></li>
                    <li class="active"><a href="./preguntas/index.php">Preguntas</a></li>
                    <li class="active"><a href="./distribucion/index.php">Distribución</a></li>
                </ul>
            </div>
        </div>
    <!-- Fin de la Barra de Navegación-->
    <!-- Header -->
        <div class="jumbotron">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-8">            
                        <h2>III Jornadas de Enseñanza de la Matemática</h2>
                        <p>Departamento de Matemática - Facultad de Ciencias Exactas - UNSa.</p>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <img class="img-responsive" alt="logo curso" src="../img/logo2018.png" width="200"></div>
              </div>
            </div>
        </div>
        <!--/container -->
        <h1> BIENVENIDOS AL SISTEMA DE ADMINISTRACION JEM 2018</h1>

    </tbody>
    </table>




 <?php include("./main/footer.php"); ?>