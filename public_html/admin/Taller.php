<?php
require('conexion.php');

Class Taller extends Conexion{
	public function Taller(){
		parent::__construct();
	}

	public function getTalleres(){
		$talleres=$this->conexion_db->query('SELECT * FROm talleres');
		return $talleres;
	}

	public function getTaller($id){
		$taller=$this->conexion_db->query('SELECT count(id) AS cant FROM talleres WHERE propuesta_id='.$id );
		$cant=$taller->fetch_array();
		return $cant['cant'];
	}

	public function creaTalleres(){
		$consulta='SELECT p.id, count(e.estado) AS cant,p.abstract,p.titulo,p.prerequisito,p.cupo
				FROM propuestas AS p 
				INNER JOIN propuestasjurados AS pj ON pj.propuesta_id=p.id 
				INNER JOIN evaluaciones AS e ON pj.id=e.propuestajurado_id
				WHERE e.estado like "%aprobado%"
                GROUP BY p.id';
		$propuestas=$this->conexion_db->query($consulta);

		while ($fila=mysqli_fetch_array($propuestas) ){
			if ($fila['cant']>=2 AND $this->getTaller($fila['id'])==0) {
					$consulta2='INSERT INTO talleres(abstract, titulo,prerequisito,cupo,propuesta_id) VALUES ("'.$fila['abstract'].'","'.$fila['titulo'].'","'.$fila['prerequisito'].'",'.$fila['cupo'].','.$fila['id'].')';
					echo $consulta2."<br>";
					$this->conexion_db->query($consulta2);
			}
		}
	}

	public function getOferentes($id){
		$oferentes=$this->conexion_db->query('SELECT * FROM oferentes AS o INNER JOIN oferentespropuestas AS op ON o.id=op.oferente_id WHERE op.propuesta_id='.$id);
		return $oferentes;	
	}
}

?>