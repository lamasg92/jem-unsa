<?php
require('../Propuesta.php');
$propuestas=new Propuesta();
if (isset($_GET["id"]) and $_GET["id"]<>""){
$codigo = $_GET["id"];
}
$propuesta=$propuestas->getPropuestaId($codigo);

include('../main/index.php');
?>
<div class="col-md-8 col-md-offset-2">
<form action="update.php" method="post">
<fieldset>
	<legend>Puede modificar los datos de este registro:</legend>

<label class="col-md-3 control-label">Titulo:</label>
<div class="col-md-8">
<?php 
echo '<input class="form-control" name="titulo" type="text" value="'.$propuesta['titulo'].'" >';
echo '<input name="id" type="hidden" value="'.$propuesta['id'].'">' ?>
</div>

<label class="col-md-3 control-label" >Titulo Corto:</label>
	<div class="col-md-8">
<?php echo '<input class="form-control" name="titcorto" type="text" value="'.$propuesta['titcorto'].'" >' ?>
	</div>

<label class="col-md-3 control-label" > Abstract:</label>
	<div class="col-md-8">
<?php echo '<textarea class="form-control" rows="10" cols="60" name="abstract">'.$propuesta['abstract'].'"</textarea>' ?>
	</div>

<label class="col-md-3 control-label" >Pedidos:</label>
	<div class="col-md-8">
<?php echo '<input class="form-control" name="pedidos" type="text" value="'.$propuesta['pedidos'].'" >' ?>
	</div>

<label class="col-md-3 control-label" >PreRequisitos:</label>
	<div class="col-md-8">
<?php echo '<input class="form-control" name="prerequisito" type="text" value="'.$propuesta['prerequisito'].'" >' ?>
	</div>
<br>
<label class="col-md-3 control-label ">Enlace:</label>
	<div class="col-md-8">
<?php echo '<a class="form-control" target="_blank" href="../../aceptados/'.$propuesta['documento'].'">Documento</a>' ?>
	</div>
<br>
<label class="col-md-3 control-label">Participantes:</label>
	<div class="col-md-8">
<?php echo '<input class="form-control" name="cupo" type="number" value="'.$propuesta['cupo'].'" >' ?>
	</div>

<label class="col-md-3 control-label">Estado:</label>
	<div class="col-md-8">
<?php echo '<select class="form-control" name="estado" value="'.$propuesta['estado'].'">
		<option value="aceptado">Aceptado</option>
		<option value="rechazado">Rechazado</option>
	</select>' ?>
	</div>
<p>

</p>
<br>
<input class="btn btn-primary" type="submit" name="Submit" value="Guardar cambios" >
</fieldset>
</form>
</div>

