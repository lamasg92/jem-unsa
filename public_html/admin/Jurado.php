<?php
require('conexion.php');

Class Jurado extends Conexion{
	public function Jurado(){
		parent::__construct();
	}

	public function asignaPass(){
		$jurados=$this->conexion_db->query('SELECT * FROM jurados');
		   while ($jurado=mysqli_fetch_array($jurados) ) {
		   		$dni=$jurado['dni'];
		   		$id=$jurado['id'];
		   		$pass=($dni+20180801)*(39-$id);
				$consulta='UPDATE jurados SET pass='.$pass.' WHERE id='.$id;
				$query=$this->conexion_db->query($consulta);
			}
	}

	public function pass($pass){
		$query=$this->conexion_db->query('SELECT * FROM jurados WHERE pass='.$pass);
		$des=$query->fetch_array();
		$dni=($pass/(39-$des['id']))-20180801;
		return $dni;
	}

	public function getJurado(){
		$jurados=$this->conexion_db->query('SELECT * FROM jurados');
		return $jurados;
	}

	public function getJuradoDNI($dni){
		$query=$this->conexion_db->query('SELECT * FROM jurados WHERE dni='.$dni);
		$jurado=$query->fetch_array();
		return $jurado;
	}

	public function getPropuestasJurado($dni){
		$consulta= 'SELECT p.id, p.abstract, p.documento,p.titulo,p.prerequisito,p.titcorto,pj.evaluar FROM jurados as j inner join propuestasjurados as pj  on j.id=pj.jurado_id inner join propuestas as p on pj.propuesta_id=p.id WHERE j.dni='.$dni;
		$propuestas=$this->conexion_db->query($consulta);
		return $propuestas;

	}
	public function getEvaluaciones($id){
		$consulta= 'SELECT  p.abstract, p.documento,p.titulo,p.prerequisito,p.titcorto,e.id, e.estado FROM jurados as j inner join propuestasjurados as pj on j.id=pj.jurado_id inner join propuestas as p on pj.propuesta_id=p.id inner join evaluaciones as e on pj.id=e.propuestajurado_id WHERE j.id='.$id ;
		$propuestas=$this->conexion_db->query($consulta);
		return $propuestas;

	}

	public function Respondidos($id){
		$consulta='SELECT count(e.id) as c FROM jurados AS j INNER JOIN propuestasjurados AS pj ON j.id=pj.jurado_id INNER JOIN evaluaciones AS e ON e.propuestajurado_id=pj.id where j.id='.$id;
		$query=$this->conexion_db->query($consulta);
		$cant=$query->fetch_array();
		return $cant;
	}
}

?>