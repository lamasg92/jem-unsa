<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"utf-8">
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="keywords" content="Jornadas de Enseñanza de la Matemática, Educación, Salta">
        <meta name="description" content="Pagina con información sobre las Jornadas de Enseñanza de la Matemática. Agosto 2018.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        
       <!-- <link rel="shortcut icon" href="./img/logoCris.ico"> -->

        <title>III Jornadas de Ense&ntilde;anza de la Matem&aacute;tica</title>
        <!-- CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="../css/bootstrap-social.css">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
            
        <script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "5a95d64d-01e6-457d-a708-52d2053032eb", 
doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
        
    
    </head>
    
    

    <body>
    
    

    <!-- Barra principal de Navegación -->
        <div class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="../index.html">Principal</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <!-- Fin de la Barra de Navegación-->
    <!-- Header -->
        <div class="jumbotron">
            <div class="container">
				<div class="row">
					<div class="col-md-8 col-xs-8">            
                        <h1>III Jornadas de Enseñanza de la Matemática</h1>
                        <p>Departamento de Matemática - Facultad de Ciencias Exactas - UNSa.</p>
                        <p>Del 1 al 3 de agosto de 2018</p>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <img class="img-responsive" alt="logo curso" src="../img/logo2018.png" width="400">                    </div>
              </div>
            </div>
        </div>
   <!-- Fin Header -->
   
    <!-- Contenido de la página actual-->
       <!-- Contenido de la página actual-->
        <div class="row">
            <div class="col-md-10 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 align="center"><p align="center">Talleres JEM 2018</p></h2>
                    </div>
                    <!-- Modificar Inicio-->
                    <div class="panel-body">
                    <?php
                    require('../admin/Taller.php');
                    $taller= new Taller();
                    $talleres=$taller->getTalleres();
 
                    while ($fila=mysqli_fetch_array($talleres) ){
                        echo '<br><div class="panel panel-success">';
                        echo '<div class="panel-heading"><h3><p align="center">'.$fila['titulo'].'<p></h3></div><br>';
                        echo '<p align="justify">'.$fila['abstract'].'</p><br>';
                        echo '<b>Requisito del Participante:</b>'.$fila['prerequisito'];
                        echo '<br><b>Cupo:</b>'.$fila['cupo'];
                        $oferente=$taller->getOferentes($fila['propuesta_id']);
                        echo "<br><b>Oferentes:</b>";
                        while ($fila1=mysqli_fetch_array($oferente)) {
                              echo '- '.$fila1['nombre'].' -';
                        }
                        echo "</div>";

                    }
                    ?>

								  
                     </div>                
                    <!-- Modificar Fin-->
                </div>
            </div>
           
    <!-- Fin del Contenido -->


    
     <div class="row">
            <div class="col-md-10 col-xs-12">
                <div class="panel panel-success">
                  <div class="panel-heading"><h3 align="center"><p align="center">Sitios relacionados con estas jornadas</p></h3>
                  </div> 
                  <div class="panel-body">
					<div class="item">

<div class="row-centered">
<div class="row">
  <div class="col-sm-4"><a href="http://www.unsa.edu.ar" target="_blank"><a href="http://www.unsa.edu.ar" target="_blank"><img src="../img/unsa.png" alt="logo unsa" width="237"></a></div>
  <div class="col-sm-4"><a href="http://matematica.exa.unsa.edu.ar/" target="_blank">
        <img src="../img/2.png" alt="logo" width="237">                                </a></div>
  <div class="col-sm-4"><a href="../files/Auspicio de la Secretaría de Planeamiento Educativo.pdf" target="_blank">
        <img src="../img/res.png" alt="logo" width="237">                                </a></div>
    <div class="col-sm-4"><a href="../files/res-cd-115-18.pdf" target="_blank">
        <img src="../img/res2.png" alt="logo" width="237">                                </a></div>
  <div class="col-sm-4">
        <a href="../files/R-CCI-2017-0060.pdf" target="_blank"><img src="../img/res3.png" alt="logo" width="237"></a> </div>
  <div class="col-sm-4"></div>
</div>
</div>
</div>


                  </div>
                   
                    <!-- Modificar Fin-->
                </div>
               
            </div>
            
    <!-- Fin del Contenido -->
<div class="col-md-10 col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading"><h3 align="center">
                      <p align="center">Auspician</p>
                    </h3>
                    </div> 
                     <!-- <div class="panel-heading">
                        <h2>Inicio</h2>
                    </div> -->
                    <!-- Modificar Inicio-->
                  <div class="panel-body">
					<div class="item">

<div class="row-centered">
<div class="row">
  <div class="col-sm-4"><a href="http://www.willkaturismo.tur.ar/" target="_blank">
        <im g src="../img/willkaTur.png" alt="logo" width="143"></a></div>
  <div class="col-sm-4"><a href="https://www.google.com.ar/maps/place/Dental+Salud+y+Estetica/@-24.7851716,-65.4081389,15z/data=!4m5!3m4!1s0x0:0x835d8db7a6f215cd!8m2!3d-24.7851716!4d-65.4081389" target="_blank">
        <im g src="img/dent.jpg" alt="logo" width="190"></a></div>
  <div class="col-sm-4">
    <p><a href="http://www.simesen-de-bielke.com/" target="_blank">
      <im g src="../img/biel.jpg" alt="logo" width="140"></a></p>
    <p>&nbsp;</p>
  </div>
  <div class="col-sm-4"><a href="https://www.google.com.ar/search?q=rayuela%20libreria&rlz=1C1QJDB_enAR641AR641&oq=rayuela&aqs=chrome.0.69i59j69i57j69i60j0l3.1055j0j7&sourceid=chrome&ie=UTF-8&safe=off&npsic=0&rflfq=1&rlha=0&rllag=-24811635,-65418657,2456&tbm=lcl&rldimm=539365006769974474&ved=0ahUKEwjWgNuK7bPVAhVDgJAKHUWtC_MQvS4ISTAA&rldoc=1&tbs=lrf:!2m4!1e17!4m2!17m1!1e2!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:10" target="_blank"><im g src="../img/ray.jpg" alt="logo" width="150"></a></div>
  <div class="col-sm-4">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p><a href="http://www.lavelozdelnorte.com.ar/" target="_blank"><im g src="../img/lav.png" alt="logo" width="209"></a></p>
  </div>
   <div class="col-sm-4"></div>

</div>
</div>
</div>


                  </div>
                    <!-- Modificar Fin-->
                </div>
    </div>
    <!-- Fin del Contenido -->
<div class="col-md-10 col-xs-12">
  <div class="panel panel-success">
<div class="panel-heading"><h3><p align="center">Redes Sociales</p>
                    </h3>
</div> 
                     <!-- <div class="panel-heading">
                        <h2>Inicio</h2>
                    </div> -->
                    <!-- Modificar Inicio-->
    <div class="panel-body">
	  <div class="item">

<div class="row-centered">
<div class="row">
  <div class="col-sm-4 social-buttons"><a class="btn btn-social-icon btn-lg btn-facebook" href="https://www.facebook.com/jornadasdeensenanzadematematica/" target="_blank"><i class="fa fa-facebook"></i></a></div>
  <div class="col-sm-4 social-buttons"><a class="btn btn-social-icon btn-lg btn-twitter"  href="https://www.twitter.com/jornadasmatemat" target="_blank"><i class="fa fa-twitter"></i></a></div>
  <div class="col-sm-4 social-buttons"><a class="btn btn-social-icon btn-lg btn-google-plus" href="https://plus.google.com/communities/105165603717941624332" target="_blank"><i class="fa fa-google-plus"></i></a></div>
 <div class="col-sm-4"></div>
 <div class="col-sm-4"></div>
 <div class="col-sm-4"></div>
</div>
</div>
</div>
</div>
<!-- Modificar Fin-->
</div>
</div>

<div class="col-md-10 col-xs-12">
  <div class="panel panel-success">
<div class="panel-heading">
                    <h3 align="center"><p align="center">Comparte esta información</h3>
    </div> 
                     <!-- <div class="panel-heading">
                        <h2>Inicio</h2>
                    </div> -->
                    <!-- Modificar Inicio-->
    <div class="panel-body">
	  <div class="item">

<div class="row-centered">
<div class="row">
  <div class="col-sm-4 social-buttons"><span class='st_sharethis_large' displayText='ShareThis'></span></div>
  <div class="col-sm-4 social-buttons"><span class='st_facebook_large' displayText='Facebook'></span></div>
  <div class="col-sm-4 social-buttons"><span class='st_twitter_large' displayText='Tweet'></span></div>
 <div class="col-sm-4 social-buttons"><span class='st_googleplus_large' displayText='Google +'></span></div>
 <div class="col-sm-4 social-buttons"><span class='st_email_large' displayText='Email'></span></div>
 <div class="col-sm-4"></div>
</div>
</div>
</div>
</div>
<!-- Modificar Fin-->
</div>
</div>
<!-- Fin del Contenido -->
<!-- Fin Auspiciantes -->
<!-- Footer -->
  <footer class="footer">
        <div class="container">
            
        </div><!--/container -->
        </br>
        <ul>
    <div class="list-inline">
                <li><span class="copyright">III Jornadas de Enseñanza de Matemática</span> - 2018</li>
                <li><a href="mailto:jornadasmatematicasalta@gmail.com ?subject=jem 2016" ><strong>Contactanos</strong></a><span class="dot divider"> &middot;</span></li>
    </div>
    </footer>           
    <!-- fin de footer -->
</body>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-2.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</html>